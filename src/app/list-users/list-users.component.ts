import {Component, OnInit} from '@angular/core';
import {User} from '../model/user';
import {ServiceUsersService} from '../services/service-users.service';
import {UserComponent} from '../user/user.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})


export class ListUsersComponent implements OnInit {
  users: User[];
  user: User = null;
  // tslint:disable-next-line:variable-name
  search_condition: string;

  constructor(private  serve: ServiceUsersService) {
  }

  ngOnInit(): void {
    this.serve.getUserss().subscribe((data: User[]) => {
      this.users = data;
      this.user = this.serve.getProvided();
      this.serve.provide(null);

      if (this.user != null) {
        for (const u of this.users) {
          // tslint:disable-next-line:triple-equals
          if (u.id == this.user.id)
          {
            this.users[this.users.indexOf(u)] = this.user;
          }
        }
      }

    });
  }

}
