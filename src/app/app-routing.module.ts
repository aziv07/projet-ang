import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AddUserFormComponent} from './add-user-form/add-user-form.component';
import {ListUsersComponent} from './list-users/list-users.component';
import {UpdateUserComponent} from './update-user/update-user.component';
import {ListCongeComponent} from './list-conge/list-conge.component';


const routes: Routes = [
  {path: '', component: ListUsersComponent},
  {path: 'add-user', component: AddUserFormComponent},
  {path: 'update-user/:id', component: UpdateUserComponent},
  {path : 'list-conge', component : ListCongeComponent}


];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
