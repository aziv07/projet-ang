import {Component, Input, OnInit} from '@angular/core';
import {User} from '../model/user';
import {Conge} from "../model/conge";
import {CongeServiceService} from "../services/conge-service.service";
import {ServiceUsersService} from "../services/service-users.service";

@Component({
  selector: 'app-list-conge',
  templateUrl: './list-conge.component.html',
  styleUrls: ['./list-conge.component.css']
})
export class ListCongeComponent implements OnInit {
conges:Conge[];
user :User;
show:boolean;
  constructor(private  serve: CongeServiceService,private service:ServiceUsersService) { }

  ngOnInit(): void {
    this.show=false;
this.user = this.serve.getUser();
this.conges = this.user.hollidays;
  }
manageShow(){
    this.show=!this.show;
}
  Affecter_conge(c:Conge) {
    this.show=!this.show;
    this.user.hollidays.push(c);
    this.service.updateUser(this.user, this.user.id ).subscribe();
  }
}
