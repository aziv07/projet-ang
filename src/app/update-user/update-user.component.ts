import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../model/user';
import {ServiceUsersService} from '../services/service-users.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  f: FormGroup;
  id: number;
user: User;
// tslint:disable-next-line:variable-name
  constructor(private serve: ServiceUsersService, private activatedroute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.activatedroute.paramMap.subscribe(res => this.id = Number(res.get('id')) );

    this.user = window.history.state;



    this.f = new FormGroup({
      nom: new FormControl(this.user.nom, Validators.required),
      id: new FormControl(this.id),
      hollidays: new FormControl(this.user.hollidays),
      prenom: new FormControl(this.user.prenom, Validators.required),
      cin: new FormControl(this.user.cin, [Validators.required, Validators.pattern('[0-9]{8}$')]),
      email: new FormControl(this.user.email, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$')]),
      type: new FormControl(this.user.type, [Validators.required]),
      password: new FormControl(this.user.password, [Validators.required, Validators.minLength(8)]),
      phoneNumber: new FormControl(this.user.phoneNumber, [Validators.required, Validators.minLength(8)])
    });
  }

  get nom() {
    return this.f.get('nom');
  }

  get prenom() {
    return this.f.get('prenom');
  }

  get cin() {
    return this.f.get('cin');
  }

  get email() {
    return this.f.get('email');
  }

  get type() {
    return this.f.get('type');
  }

  get password() {
    return this.f.get('password');
  }

  get phoneNumber() {
    return this.f.get('phoneNumber');
  }

  // tslint:disable-next-line:typedef
  update() {
    const val = JSON.stringify(this.f.value);
    this.user = JSON.parse(val);
    this.serve.provide(this.user);
    this.serve.updateUser(this.user, this.id ).subscribe();
  }
}
