import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../model/user';
import {ServiceUsersService} from '../services/service-users.service';

@Component({
  selector: 'app-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.css']
})
export class AddUserFormComponent implements OnInit {
  form: FormGroup;
u: User;
id:number;
  constructor(private  serve: ServiceUsersService) {
  }

  ngOnInit(): void {
    this.serve.getID().subscribe((e) => {
    this.id = e.id;
    });

    this.form = new FormGroup({
      nom: new FormControl('', Validators.required),
      id: new FormControl(''),
      prenom: new FormControl('', Validators.required),
      cin: new FormControl('', [Validators.required, Validators.pattern('[0-9]{8}$')]),
      email: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$')]),
      type: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  get nom() {
    return this.form.get('nom');
  }

  get prenom() {
    return this.form.get('prenom');
  }

  get cin() {
    return this.form.get('cin');
  }

  get email() {
    return this.form.get('email');
  }

  get type() {
    return this.form.get('type');
  }

  get password() {
    return this.form.get('password');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  cancel() {
    this.form.reset();
  }

  add() {
    this.u = this.form.value;
this.u.id=this.id+1;
this.u.hollidays=[];
    this.serve.setID(this.u.id).subscribe();
    this.serve.addUsers(this.u).subscribe();
  }
}
