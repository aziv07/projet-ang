import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './app-routing.module';
import { AddUserFormComponent } from './add-user-form/add-user-form.component';
import { HomeComponent } from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ServiceUsersService} from './services/service-users.service';
import { UpdateUserComponent } from './update-user/update-user.component';
import {HttpClientModule} from '@angular/common/http';
import { ListCongeComponent } from './list-conge/list-conge.component';
import { CongeComponent } from './conge/conge.component';
import {CongeServiceService} from './services/conge-service.service';

@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent,
    UserComponent,
    AddUserFormComponent,
    HomeComponent,
    UpdateUserComponent,
    ListCongeComponent,
    CongeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ServiceUsersService, CongeServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
