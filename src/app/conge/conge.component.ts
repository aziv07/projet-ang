import {Component, Inject, Input, OnInit} from '@angular/core';
import {Conge} from '../model/conge';
import {ListCongeComponent} from '../list-conge/list-conge.component';
import {ServiceUsersService} from '../services/service-users.service';
import {User} from "../model/user";

@Component({
  selector: 'app-conge',
  templateUrl: './conge.component.html',
  styleUrls: ['./conge.component.css']
})
export class CongeComponent implements OnInit {
@Input() conge: Conge;
  @Input()u: User;
modifier:boolean;
  constructor( private  serve: ServiceUsersService) { }

  ngOnInit(): void {
    this.modifier=false;
  }

update(c: Conge){
this.u.hollidays[this.u.hollidays.indexOf(this.conge)]=c;
this.conge=c;
this.serve.updateUser(this.u,this.u.id).subscribe();

}
modif(){
    this.modifier=!this.modifier;
}
delete(){
  this.u.hollidays.splice(this.u.hollidays.indexOf(this.conge),1);

  this.serve.updateUser(this.u,this.u.id).subscribe();
}
}
