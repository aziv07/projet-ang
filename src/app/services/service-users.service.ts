import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceUsersService {
  users: User[];
  user: User;
url = 'http://localhost:3000/users/';
  constructor(private http: HttpClient) {

  }

  provide(u: User){
    this.user = u;
  }
  getProvided(){
   return  this.user;

  }
  getUser(id: number){
    return this.http.get<User>(this.url + id);
}
  getUserss() {

   return this.http.get<User[]>(this.url);

  }
  getID(){
 return   this.http.get<{id: number}>('http://localhost:3000/usersId');

  }
  setID(id: number){
   return  this.http.post('http://localhost:3000/usersId', {id: id++});

  }
  addUsers(u: User) {



    return  this.http.post(this.url , u);
  }

  deleteUser(u: User) {
    this.http.delete(this.url + u.id).subscribe();
  }

  updateUser(u, id) {


return this.http.put(this.url + id, u );
  }
}




