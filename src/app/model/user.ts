import {Conge} from './conge';

export class User {
  static usersId: number;
  nom: string;
  prenom: string;
  id: number;
  cin: number;
  type: string; // true -> Admin / false -> Employee
  email: string;
  password: string;
  phoneNumber: number;
  hollidays: Conge[];
}
