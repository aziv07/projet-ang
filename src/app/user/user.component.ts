import {Component, Inject, Input, OnInit} from '@angular/core';
import {User} from '../model/user';
import {ServiceUsersService} from '../services/service-users.service';
import {ListUsersComponent} from '../list-users/list-users.component';
import {CongeServiceService} from "../services/conge-service.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() user: User;
  @Input() searchCondition: string;
  test:string;

  constructor(private  serve: ServiceUsersService,private  service:CongeServiceService, @Inject(ListUsersComponent) private parent: ListUsersComponent) {
  }

  ngOnInit(): void {
    this.test=this.user.nom.toLowerCase()+' '+this.user.prenom.toLowerCase();
  }

  delete() {
    this.serve.provide(this.user);
    this.parent.users.splice(this.parent.users.indexOf(this.user), 1);
    return this.serve.deleteUser(this.user);
  }
  provide(u:User){

    this.service.provideUserToConge(u);
  }
}
